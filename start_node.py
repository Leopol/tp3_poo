#!/usr/bin/env python3
# coding: utf-8
"""
Created on Jan 25 2021

"""

from __future__ import absolute_import
from chained_list import ChainedList


def insert_after():
    """ Inserting a node after a selected node """
    print ("Insertion node 9 after node 6")
    chained_list.insert_node_after(6,9)
    print(chained_list)


def delete():
    """ Deletion of a selected node """
    print("Deletion node 12")
    chained_list.delete_node(12)
    print(chained_list)


def insert_start():
    """ Inserting a node at the beginning of the chained list """
    print("Insertion node 0 at the beginning")
    chained_list.insert_node_start(0)
    print(chained_list)


def insert_before():
    """ Inserting a node before a selected node """
    print("Insertion node 3 before node 5")
    chained_list.insert_node_before(5,3)
    print(chained_list)


# def insert_end():
#     """ Inserting a node at the end of the chained list """
#     print("Insertion node 48 at the end")
#     chained_list.insert_node_start(63)
#     print(chained_list)


if __name__ == "__main__":
    print ("The chained start list :")
    chained_list = ChainedList([1,5,6,12,34])
    print(chained_list)
    insert_after()
    delete()
    print("Bonus fonctions :")
    insert_start()
    insert_before()
    # print("Non-functional function !")
    # insert_end()
