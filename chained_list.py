#!/usr/bin/env python3
# coding: utf-8
"""
Created on Jan 25 2021

General sources :
https://realpython.com/linked-lists-python/
https://stackabuse.com/linked-lists-in-detail-with-python-examples-single-linked-lists/

"""

from __future__ import absolute_import
from node import Node

class ChainedList:
    """
    Chained list Object
    Many methods for inserting (before, after, start, end) and
    deleting nodes/pointers on the linked list
    ----------
    nodes : list
        list that we want to transfert in a chained list of Node object
    """

    def __init__(self, chained_list=None):
        """
        This function initializes our chained list and allows us to browse
        it starting from the first node and its pointer to the next node.
        ----------
        liste : string
        """
        # First node, element of the chained list placed in a variable
        self.first_node = Node(chained_list[0])
        first = self.first_node
        # For each item/node in the chained list,
        # it behaves as if it were pointing to the next one.
        for one in chained_list[1:]:
            first.pointer = Node(one)
            first = first.pointer


    def __str__(self):
        """
        Allows us to display the chained list as a string
        and to browse through it.
        This method is linked to Node's __str__ function,
        which creates our chained list and contributes its characteristics.
        """
        return str(self.first_node)


    def insert_node_after(self, target_data, new_node):
        """
        Inserting a new node between 2 nodes :
            - Insertion after an existing node
            - Insertion before an existing node
        --> Insert a new node after the node with the value == data
        You browse the list of links/pointers to find the desired data
        and insert the new node after the node of interest.
        We readjust the links in order to rebuild the chained list.

        Warning : There can be exceptions if the list is empty or
        if the searched data does not exist, the node is non-existent.
        ----------
        data : searched data
        new_node : node to insert
        """
        # Definition of a variable to browse the chained list
        node_ia = self.first_node
        # As long as it is not null and only if it corresponds
        # to our node of interest, we keep its pointer
        while node_ia is not None:
            if node_ia.data == target_data:
                break
            node_ia = node_ia.pointer

        # Creation of the new node
        new_node = Node(new_node)
        # The pointer of the new node takes the pointer of
        # the previously searched node of interest
        new_node.pointer = node_ia.pointer
        # The pointer of our searched node indicates the head of the new node
        node_ia.pointer = new_node

        ## Order for the exception
        #if not self.first_node :
            #raise Exception("List is empty")

        ## Message of the exception
        #raise Exception("Node with data '%s' not found" % data)

        ## Other cases of exception
        #if n is None :
        #    print ("THis node is not in the list")
        #else :
        #    new_n = Node(new_node)
        #    new_n.pointer = n.pointer
        #    n.pointer = new_n


    def delete_node(self, target_data):
        """
        Delete all node(s) value == data
        ----------
        data : searched data to delete
        """
        # Definition of a variable to browse the chained list
        node_d = self.first_node
        # The pointer of the searched/interested node is selected
        if node_d.data == target_data:
            node_d = node_d.pointer
            return

        node_d = self.first_node
        # As long as the selected pointer is defined (is not null)
        # and if the pointer data indicates the searched data
        # then the selected pointer is assigned to the destination
        # of the searched node pointer.
        while node_d.pointer is not None:
            if node_d.pointer.data == target_data:
                break
            node_d = node_d.pointer
        # The node of interest is skipped
        node_d.pointer = node_d.pointer.pointer


    def insert_node_start(self, node):
        """
        Inserting a node at the beginning of the list.
        You don't have to browse the whole list.
        Creating a new node and pointing to the beginning of the list.
        ----------
        node : object containing a value and a pointer,
        insertion at the beginning of the chained list
        """
        # Creation of the new node
        new_node = Node(node)
        # Inserting the new node at the beginning of the list
        # (pointer to the head of the first node)
        new_node.pointer = self.first_node
        # The first node thus becomes the new inserted node
        self.first_node = new_node


    def insert_node_before (self, target_data, new_node):
        """
        Inserting a new node between 2 nodes :
            - Insertion after an existing node
            - Insertion before an existing node
        --> Insert a new node before the node with the value == data
        You browse the list of links/pointers to find the desired data
        and insert the new node before the node of interest.
        We readjust the links in order to rebuild the chained list.

        Warning : There can be exceptions if the list is empty or
        if the searched data does not exist, the node is non-existent.
        ----------
        data : searched data
        new_node : node to insert
        """
        # Definition of a variable to browse the chained list
        node_ib = self.first_node
        # If the head of the node corresponds to the searched node,
        # a new node is created whose pointer is directed to the head
        # of the node of interest.
        if target_data == node_ib.data:
            new_node = Node(new_node)
            new_node.pointer = node_ib
            node_ib = new_node
            return

        node_ib = self.first_node
        # As long as the pointer is not undefined and if it points
        # to the data being searched for; the pointer is retained and
        # assigned to the head of the newly created node.
        while node_ib.pointer is not None:
            if node_ib.pointer.data == target_data:
                break
            node_ib = node_ib.pointer
        new_node = Node(new_node)
        new_node.pointer = node_ib.pointer
        node_ib.pointer = new_node


    # def insert_node_end(self, node): # Non-functionnal
    #     """
    #     Inserting a node at the end of the list.
    #     You must necessarily go through the whole list.
    #     Creation of a new node whose last pointer of the list
    #     is on the head of this new node.
    #     ----------
    #     node : object containing a value and a pointer,
    #     insertion at the end of the chained list
    #     """
    #     # First test
    #     new_node = Node(node)
    #     if self.first_node is None:
    #         self.first_node = new_node
    #         new_node.pointer = None
    #         return

    #     node_ie = self.first_node
    #     while node_ie.pointer is None:
    #         node_ie = node_ie.pointer
    #     node_ie.pointer = new_node

    #     # Second test
    #     if not self.first_node:
    #         self.first_node = node
    #         return
    #     for this_node in self:
    #         pass
    #     this_node.pointer = node
