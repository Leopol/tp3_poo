#!/usr/bin/env python3
# coding: utf-8
"""
Created on Jan 25 2021

"""

from __future__ import absolute_import

class Node:
    """
    Definition of the nodes constituting the objects of a chained list
    """

    def __init__(self, param_data):
        """
        Initialization of the parameters used.
        This is the first function that will be called.
        ----------
        A node is defined by 2 properties :
        data = the data of a node, e.g. its value
        pointer = the link, the destination of the node
        """
        self.data = param_data
        self.pointer = None


    def __str__(self):
        """
        This function builds the chained list and configures the data
        and pointer parameters when creating new nodes in the insertion
        and deletion methods.
        It is indeed called several times in the ChainedList class.
        ----------
        It returns the chained list as a string in order to browse
        it and perform the described methods.
        """
        chained_list = []
        node = self
        while node.pointer is not None:
            chained_list.append(node.data)
            node = node.pointer
        chained_list.append(node.data)
        return str(chained_list)
